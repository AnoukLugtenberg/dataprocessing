SAMPLES = ["A", "B", "C"]

from snakemake.utils import report

rule all:
	input:
		"report.html"
		
rule bwa_map:
    input:
        "data/genome.fa",
        "data/samples/{sample}.fastq"
    output:
        "mapped_reads/{sample}.bam"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"

rule samtools_sort:
	input:
		"mapped_reads/{sample}.bam"
	output:
		"sorted_reads/{sample}.bam"
	shell:
		"samtools sort -T sorted_reads{wildcards.sample} "
		"-O bam {input} > {output}"
		
rule samtools_index:
	input: 
		"sorted_reads/{sample}.bam"
	output:
		"sorted_reads/{sample}.bam.bai"
	shell:
		"samtools index {input}"

rule bcftools_call:
    input:
        fa="data/genome.fa",
        bam=expand("sorted_reads/{sample}.bam", sample=SAMPLES),
        bai=expand("sorted_reads/{sample}.bam.bai", sample=SAMPLES)
    output:
        "calls/all.vcf"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"

rule report:
	input:
		"calls/all.vcf"
	output:
		html = "report.html"
	run:	
		with open(input[0]) as f:
			n_calls = sum(1 for line in f if not line.startswith("#"))
            
		report("""
		===================================
		An example workflow
		===================================
		
		Reads were mapped to the Yeas reference genome 
		and variants were called jointly with
		SAMtools/BCFtools.
		
		This resulted in {n_calls} variants (see Table T1_).
		""", output.html, metadata="Author: Anouk Lugtenberg", T1=input[0])

rule clean:
	shell:
		'rm *.html'

